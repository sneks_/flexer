@extends("templates.layout")

@section("content")
    @foreach($exercises as $exercise)
        <div>
            <h2>{{$exercise->title}}</h2>
            <p>{{Str::limit($exercise->description, 150, '...')}}</p>
        </div>
    @endforeach
@stop