<nav class='navbar navbar-default' role='navigation'>
    <div class='container'>
        <div class='navbar-header'>
            <a class='navbar-brand' href='{{ route("home") }}'>Flexer</a>
        </div>

        <div class='collapse navbar-collapse'>
            <ul class='nav navbar-nav'>
                <li><a href='{{ route("home") }}'>Home</a></li>
                <li><a href='{{ "" }}'>Exercises</a></li>
            </ul>

            <ul class='nav navbar-nav navbar-right'>
                @if(Auth::check())
                    <li><a>{{ Auth::user()->name }}</a></li>

                    @role("Teacher")
                        <li><a href='{{ "" }}'>Create exercise</a></li>
                    @endrole

                    <li><a href='{{ route("logout") }}'>Logout</a></li>

                @else
                    <li><a href='{{ route("login") }}'>Login</a></li>
                    <li><a href='{{ route("register") }}'>Register</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
