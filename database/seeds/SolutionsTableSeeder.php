<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Exercise;
use App\Status;

class SolutionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $student1 = User::where('email', '=', 'student1@student1.com')->first();
        $student2 = User::where('email', '=', 'student2@student2.com')->first();

        $unchecked = Status::where('value', '=', 0)->first();

        //fizzbuzz
        /**
         * fuckery below is working because we're usng PHP7.3
         * 
         * @see https://wiki.php.net/rfc/flexible_heredoc_nowdoc_syntaxes
         */
        $fizzbuzz = Exercise::where('title', '=', 'FizzBuzz')->first();

        $student1->solutions()->create([
            'exercise_id' => $fizzbuzz->id,
            'status_id' => $unchecked->id,
            'content' =>
<<<'CONTENT'
foreach(range(1, 100) as $value) {
    $divisibleBy3 = ($value % 3) === 0;
    $divisibleBy5 = ($value % 5) === 0;
    if ($divisibleBy3) {
        echo 'fizz';
    }
    elseif ($divisibleBy5) {
        echo 'buzz';
    }
    elseif ($divisibleBy3 && $divisibleBy5) {
        echo 'fizzbuzz';
    }
}
CONTENT
        ]);

        $student2->solutions()->create([
            'exercise_id' => $fizzbuzz->id,
            'status_id' => $unchecked->id,
            'content' => 
<<<'CONTENT'
foreach(range(1, 100) as $value) {
    $divisibleBy3 = (floor($value / 3) * 3) === $value;
    $divisibleBy5 = (floor($value / 5) * 5) === $value;
    if ($divisibleBy3) {
        echo 'fizz';
    }
    elseif ($divisibleBy5) {
        echo 'buzz';
    }
    elseif ($divisibleBy3 && $divisibleBy5) {
        echo 'fizzbuzz';
    }
}
CONTENT
        ]);
    }
}
