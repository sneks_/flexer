<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $studentRole = config('roles.models.role')::where('name', '=', 'Student')->first();
        $teacherRole = config('roles.models.role')::where('name', '=', 'Teacher')->first();
        $adminRole = config('roles.models.role')::where('name', '=', 'Admin')->first();
        $permissions = config('roles.models.permission')::all();

        /*
         * Add Users
         *
         */
        if (config('roles.models.defaultUser')::where('email', '=', 'admin@admin.com')->first() === null) {
            $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'Admin',
                'email'    => 'admin@admin.com',
                'password' => bcrypt('password'),
            ]);

            $newUser->attachRole($adminRole);
        }

        if (config('roles.models.defaultUser')::where('email', '=', 'teacher@teacher.com')->first() === null) {
            $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'Teacher',
                'email'    => 'teacher@teacher.com',
                'password' => bcrypt('password'),
            ]);
            $newUser->attachRole($teacherRole);
        }

        if (config('roles.models.defaultUser')::where('email', '=', 'student1@student1.com')->first() === null) {
            $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'Student1',
                'email'    => 'student1@student1.com',
                'password' => bcrypt('password'),
            ]);

            $newUser->attachRole($studentRole);
        }

        if (config('roles.models.defaultUser')::where('email', '=', 'student2@student2.com')->first() === null) {
            $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'Student2',
                'email'    => 'student2@student2.com',
                'password' => bcrypt('password'),
            ]);

            $newUser->attachRole($studentRole);
        }
    }
}
