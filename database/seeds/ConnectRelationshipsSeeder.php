<?php

use Illuminate\Database\Seeder;

class ConnectRelationshipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Get Available Permissions.
         */
        $permissions = config('roles.models.permission')::all();

        /**
         * Attach Permissions to Roles.
         */
        $roleAdmin = config('roles.models.role')::where('name', '=', 'Admin')->first();
        $roleTeacher = config('roles.models.role')::where('name', '=', 'Teacher')->first();
        $roleStudent = config('roles.models.role')::where('name', '=', 'Student')->first();
        foreach ($permissions as $permission) {
            if ($permission['level'] > 1) {
                $roleTeacher->attachPermission($permission);
            }
            else {
                $roleStudent->attachPermission($permission);
            }
            $roleAdmin->attachPermission($permission);
        }
    }
}
