<?php

use Illuminate\Database\Seeder;
use App\Exercise;
use App\User;

class ExercisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacher = User::where('email', '=', 'teacher@teacher.com')->first();

        $fizzbuzz = $teacher->exercises()->create([
            'title' => 'FizzBuzz',
            'description' => 'In a loop from 1 to 100, if the current number is divisible by 3 print \'fizz\', if it\'s divisible by 5 print \'buzz\', if it\'s divisible by both 3 and 5 print \'fizzbuzz\'.'
        ]);
        $christmasTree = $teacher->exercises()->create([
            'title' => 'Christmas Tree',
            'description' => 'Create an ASCII christmas tree out of spaces and asterisks'
        ]);
    }
}
