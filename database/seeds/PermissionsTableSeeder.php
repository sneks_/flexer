<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Permission Types
         *
         */
        $Permissionitems = [
            [
                'name'        => 'Can Create Exercises',
                'slug'        => 'create.exercises',
                'description' => 'Can create exercises',
                'model'       => 'Permission',
                'level'       => 2
            ],
            [
                'name'        => 'Can Create Solutions',
                'slug'        => 'create.solutions',
                'description' => 'Can create solutions',
                'model'       => 'Permission',
                'level'       => 1
            ],
            [
                'name'        => 'Can Edit Solutions',
                'slug'        => 'edit.solutions',
                'description' => 'Can edit solutions',
                'model'       => 'Permission',
                'level'       => 1
            ],
            [
                'name'        => 'Can Create Rating',
                'slug'        => 'create.rating',
                'description' => 'Can create rating',
                'model'       => 'Permission',
                'level'       => 1
            ],
        ];

        /*
         * Add Permission Items
         *
         */
        foreach ($Permissionitems as $Permissionitem) {
            $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem['slug'])->first();
            if ($newPermissionitem === null) {
                $newPermissionitem = config('roles.models.permission')::create([
                    'name'          => $Permissionitem['name'],
                    'slug'          => $Permissionitem['slug'],
                    'description'   => $Permissionitem['description'],
                    'model'         => $Permissionitem['model'],
                ]);
            }
        }
    }
}
