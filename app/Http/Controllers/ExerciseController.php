<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exercise;

class ExerciseController extends Controller
{
    public function one($id, Request $request)
    {
        $exercise = Exercise::find($id);

        if($exercise) {
            return view('exercise.one', compact('exercise'));
        }

        return redirect(route('home'));
    }

    public function list(Request $request)
    {
        return view('exercise.list', ['exercises' => Exercise::all()]);
    }
}
