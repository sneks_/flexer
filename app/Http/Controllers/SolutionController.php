<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solution;

class SolutionController extends Controller
{
    public function compare($id, $compare_id, Request $request)
    {
        $solutions = [
            Solution::find($id),
            Solution::find($compare_id)
        ];

        return view('solution.compare', compact('solutions'));
    }

    public function one()
    {
        
    }
}
