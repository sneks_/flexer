<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Solution extends Model
{
    use SoftDeletes;

    public function user() {
    	return $this->belongsTo('\App\User');
    }

    public function exercise() {
    	return $this->belongsTo('\App\Exercise');
    }

    public function status() {
    	return $this->belongsTo('\App\Status');
    }
}
