<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exercise extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description'
    ];

    protected $dates = ['deleted_at'];

    public function user() {
    	return $this->belongsTo('\App\User');
    }

    public function solutions() {
    	return $this->hasMany('\App\Solution');
    }
}
