<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'home',
    'uses' => 'IndexController'
]);
Route::get('/home', [
    'uses' => 'IndexController'
]);

Route::get("/register", [
    "uses" => "\App\Http\Controllers\Auth\RegisterController@getRegister",
    "as" => "register",
	"middleware" => "guest",
]);

Route::post("/register", [
	"uses" => "\App\Http\Controllers\Auth\RegisterController@postRegister",
	"middleware" => "guest",
]);

Route::get("/login", [
	"uses" => "\App\Http\Controllers\Auth\LoginController@showLoginForm",
	"as" => "login",
	"middleware" => "guest",
]);

Route::post("/login", [
	"uses" => "\App\Http\Controllers\Auth\LoginController@login",
	"middleware" => "guest",
]);

Route::get("/logout", [
	"uses" => "\App\Http\Controllers\Auth\LoginController@logout",
	"as" => "logout",
	"middleware" => "auth",
]);

Route::get("/logout", [
	"uses" => "\App\Http\Controllers\Auth\LoginController@logout",
	"as" => "logout",
	"middleware" => "auth",
]);

Route::get("/user/{id?}", [
	"uses" => "\App\Http\Controllers\UserController@one",
	"as" => "user.profile",
	"middleware" => "auth",
]);

Route::get("/exercise/{id}", [
	"uses" => "\App\Http\Controllers\ExerciseController@one",
	"as" => "exercise.one",
	"middleware" => "auth",
]);

Route::get("/solution/{id}", [
	"uses" => "\App\Http\Controllers\SolutionController@one",
	"as" => "solution.one",
	"middleware" => "auth",
]);

Route::get("/compare/{id}/{compare_id}", [
	"uses" => "\App\Http\Controllers\SolutionController@compare",
	"as" => "solution.compare",
	"middleware" => "auth",
]);
